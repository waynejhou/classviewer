﻿using LiteDB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClassViewer
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        ClassDatabase _Database;
        public MainWindow()
        {
            _Database = new ClassDatabase();
            InitializeComponent();
            _CareerCombo.ItemsSource = typeof(Career).GetEnumValues();
            Reset();
        }

        IEnumerable ClassList => _Database.ClassColle.FindAll();
        IEnumerable ClassVMList => _Database.ClassColle.FindAll()
            .Select(x =>  new ClassViewModel() { Database = _Database, ClassId = x.Id });

        private void ClassAdd_Click(object sender, RoutedEventArgs e)
        {
            var name = _NewClassName.Text.Trim();
            if (string.IsNullOrEmpty(name))
                return;
            if (!_Database.ClassColle.Exists(x => x.Name == name))
            {
                Console.WriteLine("Before insert");
                _Database.ClassColle.Insert(new ClassEntity() { Name = name });
                Console.WriteLine("After insert");
            }
            Reset();
        }

        private void StdAdd_Click(object sender, RoutedEventArgs e)
        {
            var name = _NewStdName.Text.Trim();
            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name Error");
                return;
            }

            if (_ClassCombo.SelectedValue == null)
            {
                MessageBox.Show("Class Error");
                return;
            }
            var classId = (_ClassCombo.SelectedValue as ClassEntity).Id;
            if (_CareerCombo.SelectedValue == null)
            {
                MessageBox.Show("Carrear Error");
                return;
            }
            var career = (Career)(_CareerCombo.SelectedValue);
            var numberText = _NewStdNumber.Text.Trim();
            if (!int.TryParse(numberText, out int number))
            {
                MessageBox.Show("Number Error");
                return;
            }

            if (_Database.StudentColle.Exists(x => x.Number == number))
            {
                MessageBox.Show("Student Exist");
                return;
            }
            var newStd = new StudentEntity() { Name = name, Number = number };
            _Database.StudentColle.Insert(newStd);
            _Database.ClassStudentColle.Insert(new ClassStudentRelationship()
            {
                Name = $"{classId}_{newStd.Id}",
                ClassId = classId,
                StudentId = newStd.Id,
                Career = career
            });
            Reset();
        }

        private void Reset()
        {
            Console.WriteLine("Before Reset");
            _ClassCombo.ItemsSource = ClassList;
            _ClassView.ItemsSource = ClassVMList;
            Console.WriteLine("After Reset");
        }


        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }
    }

    public class ClassViewModel
    {
        public ClassDatabase Database { get; set; }
        public int ClassId { get; set; } = 0;
        public IEnumerable Teachers
        {
            get
            {
                if (!Database.ClassColle.Exists(x => x.Id == ClassId))
                    return null;
                Console.WriteLine(ClassId);
                foreach( var c in Database.ClassStudentColle
                    .FindAll())
                    Console.WriteLine(c);
                return Database.ClassStudentColle
                    .Find(x => x.ClassId == ClassId)
                    .Where(x => x.Career == Career.Teacher)
                    .Select(x => Database.StudentColle.FindById(x.StudentId));
            }
        }

        public IEnumerable Cadres
        {
            get
            {
                if (!Database.ClassColle.Exists(x => x.Id == ClassId))
                    return null;
                return Database.ClassStudentColle
                    .Find(x => x.ClassId == ClassId)
                    .Where(x => x.Career == Career.Cadre)
                    .Select(x => Database.StudentColle.FindById(x.StudentId));
            }
        }
        public IEnumerable Students
        {
            get
            {
                if (!Database.ClassColle.Exists(x => x.Id == ClassId))
                    return null;
                return Database.ClassStudentColle
                    .Find(x => x.ClassId == ClassId)
                    .Where(x => x.Career == Career.Student)
                    .Select(x => Database.StudentColle.FindById(x.StudentId));
            }
        }
    }
}
