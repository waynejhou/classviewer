﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;

namespace ClassViewer
{
    public class ClassDatabase
    {
        public LiteDatabase Database;
        public LiteCollection<ClassEntity> ClassColle { get; set; }
        public LiteCollection<StudentEntity> StudentColle { get; set; }
        public LiteCollection<ClassStudentRelationship> ClassStudentColle { get; set; }
        public ClassDatabase()
        {
            Database = new LiteDatabase("Db.db");
            ClassColle = Database.GetCollection<ClassEntity>();
            StudentColle = Database.GetCollection<StudentEntity>();
            ClassStudentColle = Database.GetCollection<ClassStudentRelationship>();
            ClassColle.EnsureIndex(x => x.Name, true);
            StudentColle.EnsureIndex(x => x.Number, true);
            ClassStudentColle.EnsureIndex(x => x.Name, true);
        }
    }

    public class ClassEntity
    {
        [BsonId(true)]
        public int Id { get; set; }

        [BsonField("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class StudentEntity
    {
        [BsonId(true)]
        public int Id { get; set; }

        [BsonField("number")]
        public int Number { get; set; }

        [BsonField("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Number}號 {Name}";
        }
    }

    public class ClassStudentRelationship
    {
        [BsonId(true)]
        public int Id { get; set; }

        [BsonField("name")]
        public string Name { get; set; }

        [BsonField("class_id")]
        public int ClassId { get; set; }

        [BsonField("student_id")]
        public int StudentId { get; set; }

        [BsonField("career")]
        public Career Career { get; set; }
    }

    public enum Career { Student, Cadre, Teacher, }
}
